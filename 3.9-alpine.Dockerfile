FROM python:3.9-alpine
COPY assets/*.sh /opt/gosu/
RUN set -x \
    && apk add --no-cache --virtual .gosu-deps \
		dpkg \
		gnupg \
		openssl \
	&& /opt/gosu/gosu.download.sh \
    && apk del .gosu-deps \
    && rm /tmp/* -r \
    && /opt/gosu/gosu.install.sh \
    && rm -fr /opt/gosu

RUN pip install pipenv